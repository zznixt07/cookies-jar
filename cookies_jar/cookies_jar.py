import os
import json
import logging
from datetime import datetime

logger = logging.getLogger(__name__)

def get_datadir() -> str:
    import platform
    """
    # linux: ~/.local/share
    # macOS: ~/Library/Application Support
    # windows: C:/Users/<USER>/AppData/Roaming
    """

    home = os.path.expanduser('~')
    platform_name = platform.system().lower()
    if 'windows' in platform_name:
        datadir = 'AppData/Roaming'
    if 'linux' in platform_name:
        datadir = '.local/share'
    elif 'darwin' in platform_name:
        datadir = 'Library/Application Support'
    # don think we can make the dir if for some strange reason its doesn't exists
    return os.path.join(home, datadir)


class CookiesJar:

    dbName = os.path.join(get_datadir(), 'py_my_db', 'db', 'cookies_database')

    def __init__(self, namespace):
        '''
        the value passed in this constructor acts as kind of namespace for the cookie.
        The cookie name itslef should not be used because diff site could have same
        cookie name.
        '''

        self.namespace = namespace
        os.makedirs(os.path.dirname(self.dbName), exist_ok=True)

    def put_cookie(self, cookie_name: str, cookie_value: str, expiry: int = None) -> None:
        '''puts the given cookie to json file.
        usage:
         
        for cookie in self.sess.cookies:
          if cookie.name == 'sessionid':
            CookiesJar('websitename_').put_cookie(cookie.value, cookie.expires)
        '''
        if expiry is None:
            expiry = 2**31

        d = {self.namespace: {cookie_name: [cookie_value, expiry]}}
        with open(self.dbName + '.json', 'a+') as f:
            f.seek(0)
            contents = f.read()
            if contents:
                d = {**json.loads(contents), **d}
            f.seek(0)
            f.truncate()
            f.write(json.dumps(d))

    def get_cookie(self, cookie_name: str):
        '''
        usage:
                CookiesJar('websitename_').get_cookie('PHPSESSID'))
        '''

        with open(self.dbName + '.json', 'a+') as f:
            f.seek(0)
            read = f.read()
            if read == '':
                logger.debug('json file is empty')
                f.write('{}')
                return None
            # {self.namespace: {cookie_name: [cookie_value, expiry]}}
            fullDict = json.loads(read)
            cookies = fullDict.get(self.namespace, {})
            # pprint(cookies)
            cookie_info = cookies.get(cookie_name)
            if not cookies or not cookie_info:
                logger.debug('did not find key cookies in json file')
                return None
            # should be done with using cookies under 1000 seconds
            if (cookie_info[1] - datetime.now().timestamp()) < 100:
                logger.debug('cookies found but expired')
                # after poping stop iteration
                del fullDict[self.namespace][cookie_name]
                f.seek(0)
                f.truncate()
                f.write(json.dumps(fullDict))
                return None
            else:
                return cookie_info[0]
        

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger()
    logger.setLevel(10)
    # logging.disable()

    site = CookiesJar('mysitename+session')
    site.put_cookie(
        's_id',
        'long_%s3483#%#_gibbrish_473483_cookie_value',
        datetime.now().timestamp() + 100000000
    )
    print(site.get_cookie('nah_id'))
    print(site.get_cookie('s_id'))

