from setuptools import setup

setup(name='cookies_jar',
      version='0.1',
      description='',
      url='http://github.com/zznixt07',
      author='zznixt07',
      author_email='zznixt07@protonmail.com',
      license='MIT',
      packages=['cookies_jar'],
      zip_safe=False
)
